import { Platform } from 'react-native';
import { createSwitchNavigator, createMaterialTopTabNavigator, createStackNavigator, createAppContainer } from 'react-navigation';
import { MainColor, SecondryColor, white } from '../themes/colors';
import { defaultFont } from '../themes/fonts';
import HomeLanding from '../components/home/home';
import Map from '../components/map/map'
import Notification from '../components/notifications/notification'
import PatientRepo from '../components/reports/patientRepos/PatientRepo';
import { isIphoneX } from '../utils';

let paddingTop = 0;

if (Platform.OS === 'ios') {
  if (isIphoneX()) {
    paddingTop = 30;
  } else {
    paddingTop = 10;
  }
} else {
  paddingTop = 0;
}


const RootStack = createStackNavigator({
  HomeLanding: { screen: HomeLanding },
  Map: { screen: Map },
  Notification: { screen: Notification },
  PatientRepo: { screen:PatientRepo}
},
  {
    initialRouteName: 'Map',
    mode: 'modal',
    headerMode: 'none',
    navigationOptions: {
      headerStyle: {
        backgroundColor: MainColor
      },
      headerTintColor: white,
      headerTitleStyle: {
        fontFamily: defaultFont
      }
    }
  }
);
const Router = createAppContainer(RootStack);
export default Router;