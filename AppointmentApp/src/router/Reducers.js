import { combineReducers } from 'redux';
import homeReducer from '../components/home/homeReducer';
import patientRepoReducer from '../components/reports/patientRepos/PatientRepoReducer';
export default combineReducers({
  HomeLanding: homeReducer,
  PatientRepoReducer: patientRepoReducer
});
