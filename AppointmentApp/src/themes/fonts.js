import { Platform } from 'react-native';

export const defaultFont = (Platform.OS === 'ios') ? 'roboto' : 'sans-serif-light';
export const philosopherFont = (Platform.OS === 'ios') ? 'Philosopher' : 'Philosopher-Regular';
