export const MainColor = '#212121';
export const SecondryColor = '#ff5722';
export const backgroundGrey = '#eaeaea';
export const greyStone = '#484848';
export const copperPenny = '#ff8a50';
export const redBrew = '#c41c00';

export const black = '#000000';
export const white = '#f5f5f6';
export const grey = 'lightgrey';
export const darkGrey = 'grey';
export const lisaGrey = '#e9e9e9';
export const textGrey = '#565656';
export const subText = '#757576';
export const menuColor = '#071a38'

