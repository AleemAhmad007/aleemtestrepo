import React, { Component } from 'react';
import { Alert, StyleSheet, View } from 'react-native';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import * as patientRepoActions from './PatientRepoAction';
import Button from '../../../commonComponents/Button';
import GridView from '../../../commonComponents/GridView';


class PatientRepo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tableHead: ['Number Taken', 'Date', 'Clinic', 'Doctor Name'],
            tableData: []
        }
    }
    componentDidMount = () => {
        const { patientRepoActions } = this.props;
        patientRepoActions.getPatientReports().then(res=>{
            if(res){
                this.setState({tableData:res.tableData});} 
        })
    }
    onPress() {
        Alert.alert('You tapped the button!')
    }
    render() {
        const {state} = this;
        return <View style={styles.container}>
            <View style={styles.btnContainer}>
                <Button onPress={this.onPress} text={"WEEKLY"} />
                <Button onPress={this.onPress} text={"MONTHLY"} />
                <Button onPress={this.onPress} text={"ALL TIME"} />
            </View>
            <GridView tableHead={state.tableHead} tableData={state.tableData} />
        </View>;
    }
}

const styles = StyleSheet.create({
    container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
    btnContainer: {
        margin: 20,
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
});

export default connect(
    state => ({
    }),
    dispatch => ({
         patientRepoActions: bindActionCreators(patientRepoActions, dispatch)
    })
)(PatientRepo);
  