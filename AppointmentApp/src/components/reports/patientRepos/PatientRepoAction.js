import axios from 'axios';
import { config } from '../../../api/config';
const {REST_API}=config;

export const patientReportsLoaded = () => {

}
export const getPatientReports = () => dispatch=>{
    return  axios.get(REST_API.Reports.PatientRepo)
    .then(res=>{
        return res.data;
    })
    .catch(err=>{
        console.log(err);
    })
}
