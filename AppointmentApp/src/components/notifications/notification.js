import React, { Component } from 'react';
import _ from 'lodash';
import { Alert, AsyncStorage } from 'react-native'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { TitleSection, SmallGroupCard, ErrorMessage, ScreenContainer, NoItemCard } from '../../commonComponents';
import { black } from '../../themes/colors';
import * as signalR from '@aspnet/signalr';
import firebase from 'react-native-firebase';

class Notification extends Component {
    // componentDidMount() {
    //     const hubUrl = 'https://api.dlg.d.togethersupport.co.uk';
    //     const connectionHub = new signalR.HubConnectionBuilder()
    //         .withUrl(`${hubUrl}/hubs/DLGNotification`, {
    //             accessTokenFactory: () => {
    //                 return 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjEiLCJSZWZyZXNoVG9rZW4iOiJBUUFBQUFFQUFDY1FBQUFBRURneFlhV0JZeGJkVE10YnRaSGxCdWdCN2h5dld3S2N4U0JVRnJXVUczNXJkaUhlamVscmlLaWM5SHM0MjVNckEiLCJTdHJpcGVDdXN0b21lcklkIjoiY3VzX0RsdnFvbUJaTTM2WjBhIiwiZXhwIjoxNTUwMTE5OTQxLCJpc3MiOiJodHRwczovL2RpcnR5bGl0dGxlZ29ibGlucy5jb20vIiwiYXVkIjoiaHR0cHM6Ly9kaXJ0eWxpdHRsZWdvYmxpbnMuY29tLyJ9.8GAdozLuiPucPGpae7Q_kbYd-46nURpMtqbZX4MmhH8';
    //             }
    //         })
    //         .configureLogging(signalR.LogLevel.Information)
    //         .build();
    //     connectionHub
    //         .start()
    //         .then(() => console.log("connection has been establised..."))
    //         .catch(err => console.log("Error while establishing connection :("));
    //     connectionHub.serverTimeoutInMilliseconds = 10000; // 1 second
    //     connectionHub.onclose(() => {
    //         debugger;
    //         console.log(
    //             "connection closed tying to restart realtime connection within 5 seconds"
    //         );
    //         setTimeout(() => connectionHub.start(), 5000);
    //     });
    // }
    /*Add this Block (BEGIN) */
    async componentDidMount() {
        this.checkPermission();
    }

    //1
    async checkPermission() {
        const enabled = await firebase.messaging().hasPermission();
        debugger;
        if (enabled) {
            this.getToken();
        } else {
            this.requestPermission();
        }
    }

    //3
    async getToken() {
        let fcmToken = await AsyncStorage.getItem('fcmToken');
        debugger;
        if (!fcmToken) {
            fcmToken = await firebase.messaging().getToken();
            if (fcmToken) {
                debugger;
                // user has a device token
                await AsyncStorage.setItem('fcmToken', fcmToken);
            }
        }
    }

    //2
    async requestPermission() {
        try {
            await firebase.messaging().requestPermission();
            // User has authorised
            this.getToken();
        } catch (error) {
            // User has rejected permissions
            console.log('permission rejected');
        }
    }

    render() {
        const { navigation } = this.props;
        return (
            <ScreenContainer
                wantsHeader
                navigation={navigation}
            >

                <TitleSection color={black}>Welcome to Notification</TitleSection>

            </ScreenContainer>
        );
    }
}

export default connect(
    state => ({


    }),
    dispatch => ({

    })
)(Notification);
