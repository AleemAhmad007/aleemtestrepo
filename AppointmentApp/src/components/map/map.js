import React, { Component } from 'react';
import _ from 'lodash';
import { StyleSheet, Dimensions, TouchableOpacity, TouchableHighlight, Text } from 'react-native'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import MapView from 'react-native-maps';
import { TitleSection, ScreenContainer, } from '../../commonComponents';
import { black } from '../../themes/colors';
import { View } from 'native-base';

var region = {
    latitude: 37.78825,
    longitude: -122.4324,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
};

class Map extends Component {
    constructor(props) {
        super(props);
        this.state = {
            markers: [
                {
                    id: 1,
                    coordinate: {
                        latitude: 33.675822,
                        longitude: 73.066488
                    },
                    title: "Alshifa Hospital",
                    description: "This is the best place in Portland",
                },
                {
                    id: 2,
                    coordinate: {
                        latitude: 33.6791654,
                        longitude: 73.0307619,
                    },
                    title: "Ali Medical Hospital",
                    description: "This is the second best place in Portland",
                },
                {
                    id: 3,
                    coordinate: {
                        latitude: 33.6492759,
                        longitude: 73.0150016,
                    },
                    title: "Nescom Hospital",
                    description: "This is the third best place in Portland",
                },
                {
                    id: 4,
                    coordinate: {
                        latitude: 33.7042658,
                        longitude: 73.0523969,
                    },
                    title: "PIMS Islamabad",
                    description: "This is the fourth best place in Portland",
                },
            ],
            region: null
        };
    }
    componentDidMount() {
        this.watchID = navigator.geolocation.watchPosition((position) => {
            // Create the object to update this.state.mapRegion through the onRegionChange function
            let region = {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                latitudeDelta: 0.00922 * 1.5,
                longitudeDelta: 0.00421 * 1.5
            }
            // this.onRegionChange(region, region.latitude, region.longitude);
            this.setState({
                region
            });
        }, (error) => console.log(error));
    }
    componentWillUnmount() {
        navigator.geolocation.clearWatch(this.watchID);
    }
    render() {
        const { navigation } = this.props;
        return (
            <ScreenContainer
                navigation={navigation}
            >

                <TitleSection color={black}>Welcome to Map</TitleSection>
                <View style={styles.container}>
                    <MapView
                        ref={MapView => (this.MapView = MapView)}
                        style={styles.map}
                        loadingEnabled={true}
                        loadingIndicatorColor="#666666"
                        loadingBackgroundColor="#eeeeee"
                        moveOnMarkerPress={false}
                        showsUserLocation={true}
                        showsCompass={true}
                        showsPointsOfInterest={false}
                        provider="google"
                        initialRegion={this.state.region}
                    >
                        {this.state.markers.map((marker, index) => (
                            <MapView.Marker
                                key={index}
                                coordinate={marker.coordinate}
                                title={marker.title}
                                description={marker.description}
                            />
                        ))}
                    </MapView>
                </View>
            </ScreenContainer>
        );
    }
}

export default connect(
    state => ({


    }),
    dispatch => ({

    })
)(Map);
const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width * 1,
        height: Dimensions.get('window').height * 1,
    },
    map: {
        position: 'absolute',
        ...StyleSheet.absoluteFillObject,
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    }
});