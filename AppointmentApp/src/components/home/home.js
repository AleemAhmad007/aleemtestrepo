import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { TitleSection, SmallGroupCard, ErrorMessage, ScreenContainer, NoItemCard } from '../../commonComponents';
import { black } from '../../themes/colors';
import * as HomeActions from './homeActions';


class home extends Component {

  render() {
    const { navigation } = this.props;
    return (
      <ScreenContainer
        wantsHeader
        navigation={navigation}
      >

        <TitleSection color={black}>Welcome to Home</TitleSection>

      </ScreenContainer>
    );
  }
}

export default connect(
  state => ({


  }),
  dispatch => ({

  })
)(home);
