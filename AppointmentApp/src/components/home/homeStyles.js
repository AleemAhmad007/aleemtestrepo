import { Dimensions } from 'react-native';
import { SecondryColor } from '../../themes/colors';

export const containersStyle = {
    viewCardStyle: {
        height: Dimensions.get('window').height * 0.06,
        marginTop: 0,
        borderRadius: 0
    },
    viewCardItem: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    fontStyle: {
        fontSize: 18,
        color: SecondryColor
    }
};
