import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Icon, Label } from 'native-base';

const IconButton = (props) => {
  const { name, onPress, style, iconStyle, label, labelStyle } = props;

  return (
    <TouchableOpacity style={style} onPress={onPress}>
      <Icon name={name} style={iconStyle} />
      {label && <Label style={labelStyle} >{label}</Label>}
    </TouchableOpacity>
  );
};

export { IconButton };
