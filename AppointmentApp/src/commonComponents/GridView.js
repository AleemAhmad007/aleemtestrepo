import React from 'react';
import { StyleSheet } from 'react-native';
import { Table, TableWrapper, Row, Cell} from 'react-native-table-component';

const GridView = (props) => {
    return (
        <Table borderStyle={{ borderColor: 'transparent' }}>
            {props.tableHead && <Row style={styles.head} textStyle={styles.text} data={props.tableHead} />}
            {
                props.tableData && props.tableData.map((rowData, index) => (
                    <TableWrapper key={3*Math.random()} style={[styles.row, index % 2 && { backgroundColor: '#ccc' }]}>
                        {
                            rowData.map((cellData, cellIndex) => (
                                <Cell key={cellIndex} data={cellData} textStyle={styles.text}/>
                              ))
                        }
                    </TableWrapper>
                ))
            }
        </Table>
    )
};
const styles = StyleSheet.create({
    head: { height: 40, backgroundColor: '#6c7a86' },
    text: { margin: 6 },
    row: { flexDirection: 'row' }
});

export default GridView;