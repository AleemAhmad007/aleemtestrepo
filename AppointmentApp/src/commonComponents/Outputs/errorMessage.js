import React from 'react';
import { Label } from 'native-base';
import { redBrew } from '../../themes/colors';
import { defaultFont } from '../../themes/fonts';

const ErrorMessage = (props) => {
    const { error } = props;
    const { errorMessage } = styles;
    return (
      <Label style={errorMessage}>{error}</Label>
    );
};

const styles = {
  errorMessage: {
    color: redBrew,
    textAlign: 'center',
    marginTop: 10,
    fontFamily: defaultFont
  },
};

export { ErrorMessage };
