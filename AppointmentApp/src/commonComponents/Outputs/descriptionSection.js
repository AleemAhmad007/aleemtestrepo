import React from 'react';
import { Label } from 'native-base';
import { defaultFont } from '../../themes/fonts';

const DescriptionSection = (props) => {
  const { fontSize, children, color, lineHeight, marginLess = false } = props;
  let margin = 5;
  if (marginLess) margin = 0;
  return (
    <Label
      style={{
        marginTop: margin,
        fontFamily: defaultFont,
        fontSize: fontSize || 15,
        lineHeight: lineHeight || 17,
        color,
        marginLeft: 2
      }}
    >{children}</Label>
  );
};
export { DescriptionSection };
