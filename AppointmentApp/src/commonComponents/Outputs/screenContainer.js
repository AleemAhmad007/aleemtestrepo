import React, { Component } from 'react';
import { Container, Header } from 'native-base';
import { Dimensions, ScrollView, StatusBar, Alert, Platform } from 'react-native';
import { backgroundGrey, MainColor } from '../../themes/colors';
import { CCHeader } from '../../commonComponents';
import { iosMargin } from '../../utils';



class ScreenContainer extends Component {
  constructor() {
    super();
  }
  renderHeader(wantsHeader, navigation) {
    return (
      <CCHeader
        label={'Q MY TURN'}
        navigation={navigation}
      />
    )
  }
  render() {
    const {
      wantsHeader,
      navigation

    } = this.props;
    const { containerStyle } = styles;
    return (
      <Container style={containerStyle}>
        {this.renderHeader(wantsHeader, navigation)}
        <StatusBar
          backgroundColor={MainColor}
          barStyle="light-content"
        />
        <ScrollView ref={(ref) => { this._scrollView = ref; }} style={{ marginTop: iosMargin, width: Dimensions.get('window').width * 1, paddingLeft: Dimensions.get('window').width * 0.04, paddingRight: Dimensions.get('window').width * 0.04 }}>
          {this.props.children}
        </ScrollView>
      </Container>
    );
  }
}
const styles = {
  containerStyle: {
    width: Dimensions.get('window').width * 1,
    height: Dimensions.get('window').height * 1,
    backgroundColor: backgroundGrey,
    alignItems: 'center',
  }
};

export { ScreenContainer };
