import React from 'react';
import { Label } from 'native-base';
import { defaultFont } from '../../themes/fonts';

const TitleSection = (props) => {
  const { fontSize = 18, color, children, marginBottom, marginLess = false } = props;
  const margin = marginLess ? 5 : 10;
  return (
    <Label style={{ marginTop: margin, marginBottom: marginBottom || 5, fontFamily: defaultFont, color, fontSize, marginLeft: 2, backgroundColor: 'transparent' }}>{children}</Label>
  );
};

export { TitleSection };
