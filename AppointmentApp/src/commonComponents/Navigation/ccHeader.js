import React from 'react';
import { Text, Platform, Dimensions, View, TouchableOpacity } from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import { Header, Icon } from 'native-base';
import { MainColor, white, menuColor, backgroundGrey } from '../../themes/colors';
import { defaultFont, philosopherFont } from '../../themes/fonts';
import icoMoonConfig from '../../../selection.json';
import { isIphoneX } from '../../utils';
import { IconButton } from '../../commonComponents';

let height = 0;

if (Platform.OS === 'ios') {
  if (isIphoneX()) {
    height = 80;
  } else {
    height = 77;
  }
} else {
  height = 56;
}


//const Icon = createIconSetFromIcoMoon(icoMoonConfig);

const CCHeader = (props) => {
  const { containerStyle } = styles;
  const { label, navigation, settingIcon } = props;

  return (
    <View>
      <Header style={containerStyle}>
        {renderHeaderInformation(label, navigation)}

      </Header>
      {renderMenuHeaderInformation(label, navigation)}
    </View>

  );
};
const renderMenuHeaderInformation = (label, navigation, settingIcon) => {
  const { iconStyle, headerText, viewContainer, menuContainer, iconButtonStyle, menuItemStyle, menuItemCircle } = styles;

  return (
    <View style={menuContainer}>
      <View style={menuItemStyle}>
        <IconButton iconStyle={iconButtonStyle} name='md-add-circle' />
      </View>
      <View style={menuItemStyle}>
        <IconButton iconStyle={iconButtonStyle} name='md-mail' onPress={() => navigation.navigate('Notification')} />
      </View>

      <View style={menuItemCircle}>
        <Text style={{ color: 'gray' }}>Hi</Text>
        <Text style={{ color: '#217ed5' }}>Ahsan</Text>
      </View>

      <View style={menuItemStyle}>
        <IconButton iconStyle={iconButtonStyle} name='md-grid' />
      </View>
      <View style={menuItemStyle}>
        <IconButton iconStyle={iconButtonStyle} name='md-pin' onPress={() => navigation.navigate('Map')} />
      </View>
    </View>

  );
};

const renderHeaderInformation = (label, navigation, settingIcon) => {
  const { iconStyle, headerText, viewContainer, iconButtonContainer, headerIconContainer, headerIconStyle } = styles;

  return (
    <View style={iconButtonContainer}>
      <View style={viewContainer}>
        <Text style={headerText}>{label}</Text>
      </View>
      <View style={headerIconContainer}>
        <IconButton iconStyle={headerIconStyle} name='settings' />
        <IconButton iconStyle={headerIconStyle} name='share' />
        <IconButton iconStyle={headerIconStyle} name='settings' />
      </View>
    </View>

  );
};

const styles = {
  containerStyle: {
    backgroundColor: white,
    width: Dimensions.get('window').width * 1,
    alignItems: 'center',
    height
  },
  menuContainer: {
    //flex: 1,
    flexDirection: 'row',
    backgroundColor: menuColor,
    height: 70,
    marginBottom: 20
  },
  iconStyle: {
    color: white
  },
  menuItemStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuItemCircle: {
    width: 100,
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 100 / 2,
    backgroundColor: 'white',
    // backgroundColor: '#03A9F4',
    // alignItems: 'center',
    shadowOffset: { width: 0, height: 13 },
    shadowOpacity: 0.3,
    shadowRadius: 6,

    // android (Android +5.0)
    elevation: 3,
  },
  iconContainerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: Dimensions.get('window').width * 0.01,
  },
  headerIconContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  viewContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  iconButtonContainer: {
    flexDirection: 'row',
  },
  headerIconStyle: {
    color: '#217ed5',
    fontSize: 30,
    paddingLeft: 10
  },
  headerText: {
    color: 'green',
    fontFamily: philosopherFont,
    fontSize: 18,
    alignItems: 'center',
    textAlign: 'center',
    fontWeight: 'bold',
  },
  iconButtonStyle: {
    color: white,
    fontSize: 30,
  },
};

export { CCHeader };
