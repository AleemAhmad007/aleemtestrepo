import React from 'react';
import { StyleSheet } from 'react-native';
import { TouchableOpacity, Text } from 'react-native';

const Button = (props) => {
    return (<TouchableOpacity
        style={styles.button}
        onPress={props.onPress}
    >
        <Text> {props.text} </Text>
    </TouchableOpacity>
    )
};
const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        backgroundColor: '#00a77c',
        padding: 10,
        borderRadius: 10
    }
});
export default Button;